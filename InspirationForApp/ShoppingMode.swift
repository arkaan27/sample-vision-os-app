//
//  ShoppingMode.swift
//  InspirationForApp
//
//  Created by Arkaan Quanunga on 22/10/2023.
//

import SwiftUI
import RealityKit
import RealityKitContent

struct ShoppingMode: View {
    var body: some View {
        RealityView { content in
            // Add the initial RealityKit content
        }
    }
}

#Preview {
    ImmersiveView()
        .previewLayout(.sizeThatFits)
}
