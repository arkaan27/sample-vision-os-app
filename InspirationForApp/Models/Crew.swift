//
//  Crew.swift
//  InspirationForApp
//
//  Created by Arkaan Quanunga on 23/10/2023.
//

import Foundation

enum Crew : String, Identifiable, CaseIterable, Equatable
{
    case vinush, hannia, kasturi, harris, arkaan
    var id : Self { self }
    
    var fullName: String {
        switch self {
        case.vinush:
            "Vinush Vigneswaran"
        case.hannia:
            "Hannia Tahir"
        case.kasturi:
            "Kasturi Mitra"
        case.harris:
            "Harris Memon"
        case.arkaan:
            "Arkaan Quanunga"
        }
    }
    
    var about: String {
        switch self {
        case.vinush:
            "Vinush Vigneswaran is an upcoming Research Software Engineer in the Cardiology department of Edinburgh university, leader in developing heart model simulation to cure heart diseases"
        case.hannia:
            "Hannia Tahir is a former Amazon Intern & a SABI team member which takes crucial business decisions for company using strategic data analysis. She really knows her way with the businesses & is known for her genione admiration for many of her male colleagues in the workplace"
        case.kasturi:
            "Kasturi Mitra is a DevOps Engineer at Credera & loves to go for a hike. It is another story that she cannot finish them but doesn't miss out on the fun of being with her close friends"
        case.harris:
            "Harris Memon is a Software Developer for Sky & loves to go travelling. He has been born and brought up in London so one can he say he is a proper Britisher"
        case.arkaan:
            "Arkaan Quanunga is a Software Engineer for V-Nova, and suffers from dual personality disorder. He has two personalities, one being the serious and composed and the other being completely playful..Always have heads up to see which one he shows next"
        }
    }
}

