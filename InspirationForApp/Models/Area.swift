//
//  Area.swift
//  InspirationForApp
//
//  Created by Arkaan Quanunga on 23/10/2023.
//

import Foundation

enum Area : String, Identifiable, CaseIterable, Equatable
{
    case astronauts, equipment, mission
    var id : Self { self }
    var name: String { rawValue.lowercased()}
    
    var title: String {
        switch self {
        case.astronauts:
            "Inspiration for mission crew members...."
        case.equipment:
            "Inspiration for mission equipment...."
        case.mission:
            "Inspiration for mission trailer....."
        }
    }
}
