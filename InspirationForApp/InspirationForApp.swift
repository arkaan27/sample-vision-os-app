//
//  InspirationForAppApp.swift
//  InspirationForApp
//
//  Created by Arkaan Quanunga on 22/10/2023.
//

import SwiftUI

@main
struct InspirationForAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
            Areas()
        }

        ImmersiveSpace(id: "ImmersiveSpace") {
            ImmersiveView()
        }
    }
}
